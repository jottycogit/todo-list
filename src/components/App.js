import React, {Component} from 'react';
import Action from './Action';
import ActionList from './ActionList';
import '../App.css';

class App extends Component {

  state = {
    actions: []
  }

  onClick = () => {
    let actionTitle = this.refs.titleAction.value;
    if(actionTitle !== "") {
      let random_id = Math.floor(Math.random() * (100000000000000 - 1))
      let action = <Action actionDelete={this.actionDelete} actionTitle={actionTitle} actionDone={false} id={random_id.toString()} />  
      this.setState(() => {
        let oldState = JSON.parse(localStorage.getItem("actions"));
        oldState.unshift(action);
        localStorage.setItem("actions", JSON.stringify(oldState));
        return oldState;
      });
    }
    this.forceUpdate();
  }

  actionDelete = (action) => {
    let oldState = JSON.parse(localStorage.getItem("actions"));
    for(var i = 0; i < oldState.length; i++) {
      if(oldState[i].props.id === action.props.id) {
        this.state.actions.splice(i, 1);
        oldState.splice(i, 1);
        localStorage.setItem("actions", JSON.stringify(oldState));
        break;
      }
        
    }

    this.forceUpdate();
  }

  render() {
    let actions = JSON.parse(localStorage.getItem("actions"));
    if(actions === null) {
      localStorage.setItem("actions", "[]");
      actions = [];
    }

    for(var i = 0; i < actions.length; i++) {
      actions[i].props.actionDelete = this.actionDelete;
    }
    return (
      <div className="App">
        <div className="wrapper-header">
          <input ref="titleAction" className="add-text" type="text" />
          <div className="break"></div>
          <div className="create-text" onClick={this.onClick}>Создать</div>
        </div>
        <ActionList actions={actions} />
    </div>
    )
  }
}

export default App;
