import React, {Component} from 'react';
import Action from './Action';

class ActionList extends Component {
    render() {
        const actions = this.props.actions;
        const listActions = actions.map((action) => {
            return (<Action actionDelete={action.props.actionDelete} actionTitle={action.props.actionTitle} actionDone={action.props.actionDone} key={action.props.id} id={action.props.id} /> )
        }
        );
        return (
            <div>
                {listActions}
            </div>
        )
    }
}

export default ActionList;