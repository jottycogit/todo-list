import React, {Component} from 'react';
import propTypes from 'prop-types';

class Action extends Component {
    constructor(props) {
        super(props);

        this.actionDelete = this.props.actionDelete.bind(this);
        this.onDoneChange = this.onDoneChange.bind(this);

        this.state = {
            id: this.props.id,
            actionTitle: this.props.actionTitle,
            actionDone: this.props.actionDone
        }
    }

    onDoneChange = () => {
        let actions = JSON.parse(localStorage.getItem("actions"));
        actions.map(action => {
            if(action.props.id === this.state.id) {
                action.props.actionDone = !action.props.actionDone;
            }
            return 1;
        })

        localStorage.setItem("actions", JSON.stringify(actions));

        let newStatusAction = !this.state.actionDone;
        this.setState({
            actionDone: newStatusAction
        })
    }

    render() {
        return (
            <div id={this.state.id} className={ !!this.state.actionDone ? "action-wrapper action-wrapper__done" : "action-wrapper action-wrapper__not-done" }>
                <div className="actionTitle">Название: {this.state.actionTitle}</div>
                <div className="actionDone">Выполнено: <input ref="checkboxAction" type="checkbox" checked={!!this.state.actionDone} onChange={this.onDoneChange}/></div>
                <div className="actionDelete" onClick={() => this.actionDelete(this)}>Удалить</div>
            </div>
        )
    }
}

Action.propTypes = {
    id: propTypes.string.isRequired,
    actionTitle: propTypes.string.isRequired,
    actionDone: propTypes.bool.isRequired,
    actionDelete: propTypes.func.isRequired
}

export default Action;